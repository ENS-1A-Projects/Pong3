/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Collision;

import Object.Ball;
import Object.AbstractRacket;

import Engine.Field;
import static Engine.Field.height;
import static Engine.Field.width;
/*
 *
 * @author gregoirepacreau
 */
public class Bounceable{
   public static void collidesRacket(Ball ball, AbstractRacket lracket, AbstractRacket rracket)
   {
       System.out.println("coliding verified");
       if (ball.X.get()+ball.dX.get()- ball.size.get()/2. < lracket.WIDTH.get())
       {
           ball.dX.set(- ball.dX.get());
       }
       if (ball.X.get()+ball.dX.get()+ball.size.get()/2. > rracket.X.get())
       {
            ball.dX.set(- ball.dX.get());
           
       }
   }
   
   public static void collidesWall(Ball ball)
   {
       if (ball.Y.get()+ball.size.get()/2. >= Field.width)
       {
           ball.dY.set(-ball.dY.get());
       }
       if (ball.Y.get()-ball.size.get()/2. <= 0)
       {
           ball.dY.set(-ball.dY.get());
       }
   } 
   
   private static void hasWon(Ball ball)
   {
       ball.X.set(width/2.);
       ball.Y.set(height/2);
   }
}
