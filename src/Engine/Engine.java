/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Engine;

import Collision.Bounceable;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.application.Application;
import javafx.animation.Animation;
import static javafx.application.Application.launch;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.control.Button;
import javafx.event.ActionEvent;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.animation.Timeline;
import javafx.animation.KeyFrame;
//import java.time.Duration;
import javafx.animation.KeyValue;
import javafx.util.Duration;
import Object.Ball;

import Engine.Field;
import static Engine.Field.ball;
import javafx.scene.layout.StackPane;
/**
 *
 * @author dylan
 */
public class Engine 
{
    public static void animateRackets(Timeline rackets)
    {
        if (Field.keyPressed.equals(Field.lracket.up)){
            rackets.getKeyFrames().clear(); 
            rackets.stop();
            rackets.getKeyFrames().add(new KeyFrame(
                Duration.millis(10),
                new KeyValue(Field.lRacShape.translateYProperty(), Field.lRacShape.getTranslateY()-10)
                ));

            rackets.play();}
        
        if (Field.keyPressed.equals(Field.lracket.down)){
            rackets.getKeyFrames().clear();
            rackets.stop();            
            rackets.getKeyFrames().add(new KeyFrame(
                Duration.millis(10),
                new KeyValue(Field.lRacShape.translateYProperty(), Field.lRacShape.getTranslateY()+10)
                ));

            rackets.play();}
        
        if (Field.keyPressed.equals(Field.rracket.down)){
            rackets.getKeyFrames().clear();
            rackets.stop();            
            rackets.getKeyFrames().add(new KeyFrame(
                Duration.millis(10),
                new KeyValue(Field.rRacShape.translateYProperty(), Field.rRacShape.getTranslateY()+10)
                ));

            rackets.play();}
        if (Field.keyPressed.equals(Field.rracket.up)){
            rackets.getKeyFrames().clear();
            rackets.stop();            
            rackets.getKeyFrames().add(new KeyFrame(
                Duration.millis(10),
                new KeyValue(Field.rRacShape.translateYProperty(), Field.rRacShape.getTranslateY()-10)
                ));

            rackets.play();}
    };
    
    public static void animateBall(Timeline myBall)
    {
        myBall.getKeyFrames().clear();
        myBall.stop();
        System.out.println(Field.ball.dX.get());
        System.out.println(Field.ball.dY.get());
        myBall.getKeyFrames().add(new KeyFrame(
            Duration.millis(5000),
            new KeyValue(Field.ballShape.translateYProperty(), Field.ball.dY.get())));
        myBall.getKeyFrames().add(new KeyFrame(
            Duration.millis(5000),
            new KeyValue(Field.ballShape.translateXProperty(), Field.ball.dX.get())));
        myBall.getKeyFrames().add(new KeyFrame(
                Duration.millis(5000),
                e->{
                    System.out.println("Ball Y" + Field.ball.Y.get());
                    Field.ball.update();
                    System.out.println(Field.ball.Y.get());
                }
        ));
        Bounceable.collidesRacket(Field.ball, Field.lracket, Field.rracket);
        Bounceable.collidesWall(ball);
        myBall.setCycleCount(Animation.INDEFINITE);
        myBall.play();
    }
}
