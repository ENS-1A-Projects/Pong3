/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Engine;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;

import Object.Ball;
import Object.HumanControledRacket;
import Object.IARacket;
import Collision.Bounceable;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

import javafx.animation.AnimationTimer;
/**
 *
 * @author dylan
 */


public class Field extends Application
{
    static Ball ball;
    static Circle ballShape;
    static HumanControledRacket lracket;
    static Rectangle lRacShape;
    static HumanControledRacket rracket;
    static Rectangle rRacShape;
    public static String keyPressed;
    
    static Ball ball2;
    static Circle ball2Shape;
    
    public static void changeKeyPressed(String key)
        {
            keyPressed = key;
        };
    public static int height = 600;
    public static int width = 800;
    
    @Override
    public void start(Stage primaryStage)
    {
        primaryStage.setTitle("Pong");
        Group root = new Group();
        Scene scene = new Scene(root,width,height, Color.BLACK);
//        primaryStage.setFullScreen(true);
//        primaryStage.setAlwaysOnTop(true);
        
        
        ball = new Ball(width/2.,height/2.,20.);
        ballShape = new Circle(ball.size.get(), Color.WHITE);
        ballShape.setLayoutX(ball.X.get());
        ballShape.setLayoutY(ball.Y.get());
        root.getChildren().add(ballShape);
        
        ball2 = new Ball(ball.X.get()+ball.dX.get(),ball.Y.get()+ball.dY.get(),20.);
        ball2Shape = new Circle(ball.size.get(), Color.RED);
        ball2Shape.setLayoutX(ball2.X.get());
        ball2Shape.setLayoutY(ball2.Y.get());
        root.getChildren().add(ball2Shape);        
        
        lracket = new HumanControledRacket(0.,height/2., 10., 100.,"W","S");
        lRacShape = new Rectangle();
        lRacShape.setFill(Color.WHITE);
        lRacShape.setWidth(lracket.WIDTH.get());
        lRacShape.setHeight(lracket.HEIGHT.get());
        lRacShape.setLayoutX(lracket.X.get());
        lRacShape.setLayoutY(lracket.Y.get());
        root.getChildren().add(lRacShape);
        
        rracket = new HumanControledRacket(width-10.,height/2., 10., 100.,"UP","DOWN");
        rRacShape = new Rectangle();
        rRacShape.setFill(Color.WHITE);
        rRacShape.setWidth(rracket.WIDTH.get());
        rRacShape.setHeight(rracket.HEIGHT.get());
        rRacShape.setLayoutX(rracket.X.get());
        rRacShape.setLayoutY(rracket.Y.get());
        root.getChildren().add(rRacShape);
        
        keyPressed = "";
        
        Timeline rackets = new Timeline();
        Timeline theBall = new Timeline();
        
        scene.setOnKeyPressed((key) -> {
            changeKeyPressed(key.getCode().toString());
            Engine.animateRackets(rackets);
            });
        
        Engine.animateBall(theBall);
        
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }

}
