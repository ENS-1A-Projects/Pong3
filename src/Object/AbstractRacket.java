/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Object;

import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author dylan
 */
public class AbstractRacket
{
    public final DoubleProperty X = new SimpleDoubleProperty();
    public final DoubleProperty Y = new SimpleDoubleProperty();
    public final DoubleProperty WIDTH = new SimpleDoubleProperty();
    public final DoubleProperty HEIGHT = new SimpleDoubleProperty();
    
    public AbstractRacket(Double x, Double y, Double w, Double h)
    {
        X.set(x);
        Y.set(y);
        WIDTH.set(w);
        HEIGHT.set(h);
    }
    
    public static void printRacket(AbstractRacket racket)
    {
        Rectangle rac = new Rectangle();
        rac.setLayoutX(racket.X.get());
        rac.setLayoutY(racket.Y.get());
    }
}