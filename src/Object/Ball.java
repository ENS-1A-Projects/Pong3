/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Object;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import java.util.Random;
import java.lang.Math;

/**
 * Ball class
 * @author dylan
 */

public class Ball
{
    public final DoubleProperty X = new SimpleDoubleProperty();
    public final DoubleProperty Y = new SimpleDoubleProperty();
    public final DoubleProperty size = new SimpleDoubleProperty();
    public final DoubleProperty dX = new SimpleDoubleProperty();
    public final DoubleProperty dY = new SimpleDoubleProperty();

   public Ball(Double x, Double y, Double d)
    {
        Random rand = new Random();
        X.set(x);
        Y.set(y);
        size.set(d);
        //double angle = rand.nextDouble()*2.*Math.PI;
        double angle = Math.PI/2.;
        dX.set(Math.cos(angle)*100);
        dY.set(-Math.sin(angle)*100);
        
    }
   
   public void update()
   {
       X.set(X.get() + dX.get());
       Y.set(Y.get() + dY.get());
   }
}
