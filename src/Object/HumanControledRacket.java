/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Object;

import javafx.scene.input.KeyEvent;
import Object.AbstractRacket;

/**
 *
 * @author dylan
 */
public class HumanControledRacket extends AbstractRacket
{
    public String up;
    public String down;
    
    public HumanControledRacket
        (Double X, Double Y, Double W, Double H, String upKey, String downKey)
    {
        super(X, Y, W, H);
        up = upKey;
        down = downKey;
        
    }
    private static final int DELTA = 5;
    
    public static void actionOnKeyPressed(HumanControledRacket racket, KeyEvent event)
    {
        String keyPressed = event.getCode().toString();
        if (keyPressed == racket.up)
        {
            racket.Y.set(racket.Y.get()-DELTA);
        }
        if (keyPressed == racket.down)
        {
            racket.Y.set(racket.Y.get()-DELTA);
        }
    }
}
